import glob
import os
import sqlite3

from PIL import Image

# open the db and create a cursor
db = sqlite3.connect("ratios.db")
dbc = db.cursor()

for im_file in glob.glob("specimen_cutout/*.png"):
    # open the image 
    # get height and width
    img = Image.open(im_file)

    out_path = os.path.basename(im_file)
    width = img.width
    height = img.height

    # add to database
    if height > width:
        ratio = float(height) / width
    else:
        ratio = float(width) / height

    try:
        dbc.execute("INSERT INTO images VALUES (?, ?, ?, ?)", (ratio, width, height, out_path))
    except sqlite3.IntegrityError as err:
        print(err)
        print("Trying db update instead.")
        dbc.execute("UPDATE images SET (ratio, width, height) = (?, ?, ?) WHERE img_path = ?",
                    (ratio, width, height, out_path))


db.commit()
db.close()
