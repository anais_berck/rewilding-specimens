# rewilding-specimens

This is the code for the work "Rewilding Specimens" a reading machine for taxonomic collages. The portraits of historic taxonomists were used as a background structure for compositions of cut-outs from the Herbarium of the Belgian Botanical Collections database.

## Components

_image_fetch.py_
:   This script expects a CSV file called "belgian_colony_data_all.csv" where the first column is a barcode ID for a specimen in the Botanical Collections database. It will download the images and save them in a directory called "specimen_img_raw". It will sleep for 1 second in between requests to avoid taxing the server (or getting throttled). If your scrape is interrupted, it can restart where it left off.

_crop_specimens.py_
:   This script goes through all of the images in the "specimen_img_raw" directory and performs several processes. First, it attempts to remove the background with Canny edge detection. Next it searches for contours and sorts them by size. Since the images are quite diverse and contain a lot of extraneous contours, the user is shown the cut-out and asked to confirm if the contour looks like a real part of a plant. If the answer is "n", the next contour is shown. When a valid cut-out is found, the image is then rotated and pasted into a PNG fitting the minimum rectangle. A reference to the image is saved to a sqlite database "ratios.db" with the dimensions and aspect ratio.

_update_ratio_db.py_
:   This script was to repair some broken data that was not saved properly. It scans the folder "specimen_cutout" and checks to see if there is a row for each image. If not, it makes one, populated with the appropriate data.

_rewild_poster.py_
:   This script expects a set of base portrait images in the directory "portrait_src". For each of these images it builds a basic compositional structure using Hierarchical Feature Selection (HFS) to find interesting contours above 100 pixels in total area. For each of these a contour is detected and reduced to a minimum area rectangle and rotation angle. Next a search is performed in the database to find a cut-out from the herbarium with the closest aspect ratio. The image is then scaled, rotated and pasted onto the base portrait. The result is a rewilded taxonomic image.

